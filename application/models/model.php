<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

	public $variable;

	public function __construct() {
		parent::__construct();
		
	}

	//ambil data user
	function GetUser($data) {
        $query = $this->db->get_where('tb_login', $data);
        return $query;
    }

	// ambil data perusahaan
	public function getPerusahaan($where= "") {
		$data = $this->db->query('SELECT * from tb_perusahaan '.$where);
		return $data;
	}

	// ambil data pimpinan
	public function getProfil($where= "") {
		$data = $this->db->query('SELECT * from tb_profil '.$where);
		return $data;
	}

	// ambil data berita
	public function GetDataBerita($where= "") {
		$data = $this->db->query('SELECT * from tb_berita '.$where);
		return $data;
	}
	public function dataBerita($number,$offset){
		return $query = $this->db->get('tb_berita',$number,$offset)->result();		
	}
	public function dataBeritaFront($number,$offset){
		return $query = $this->db->where('status_post =', 1)->order_by('tgl_buat', 'DESC')->get('tb_berita',$number,$offset)->result();		
	}
	public function jumlahBerita(){
		return $this->db->get('tb_berita')->num_rows();
	}

	// ambil data pengumuman
	public function GetDataPengumuman($where= "") {
		$data = $this->db->query('SELECT * from tb_pengumuman '.$where);
		return $data;
	}
	public function dataPengumuman($number,$offset){
		return $query = $this->db->get('tb_pengumuman',$number,$offset)->result();		
	}
	public function dataPengumumanFront($number,$offset){
		return $query = $this->db->where('status_post =', 1)->get('tb_pengumuman',$number,$offset, 'order by tgl_buat desc')->result();		
	}
	public function jumlahPengumuman(){
		return $this->db->get('tb_pengumuman')->num_rows();
	}

	// ambil data user
	public function GetDataUser($where= "") {
		$data = $this->db->query('SELECT * from tb_login '.$where);
		return $data;
	}

	//batas crud data
	public function Simpan($table, $data) {
		return $this->db->insert($table, $data);
	}

	public function Update($table,$data,$where) {
		return $this->db->update($table,$data,$where);
	}

	public function Hapus($table,$where){
		return $this->db->delete($table,$where);
	}
}

/* End of file model.php */
/* Location: ./application/models/model.php */