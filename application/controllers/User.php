<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
	}

	public function index() {
		$data = [
			'title' => 'Data User',
			'nama' => $this->session->userdata('nama'),	
			'data_user' => $this->model->getDataUser("order by nama asc")->result_array(),
			'isi' => 'user/data.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => 'active',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function form() {
		$data = [
			'title' => 'Tambah Data',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_user' => '',
			'nama_user' => '',
			'pass_user' => '',
			'bu_pass' => '',
			'nama_lengkap' => '',
			'level' => '',
			'isi' => 'user/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => 'active',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function edit_user($kode = 0){		
		$tampung = $this->model->getDataUser("where id_user = '$kode'")->result_array();
		
		$data = [
			'title' => 'Edit Data',
			'nama' => $this->session->userdata('nama'),
			'status' => 'lama',
			'id_user' => $tampung[0]['id_user'],
			'nama_user' => $tampung[0]['nama_user'],
			'pass_user' => $tampung[0]['bu_pass'],
			'nama_lengkap' => $tampung[0]['nama'],
			'level' => $tampung[0]['level'],
			'isi' => 'user/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => 'active',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savedata(){
		if($_POST){
			$status = $_POST['status'];
			$id_user = $_POST['id_user'];
			$nama_user = $_POST['nama_user'];
			$pass_user = $_POST['pass_user'];
			$nama_lengkap = $_POST['nama_lengkap'];
			$level = $_POST['level'];
			if($status == "baru"){
				$this->load->helper(array('form', 'url'));
				$this->load->library('form_validation');
		    $this->form_validation->set_rules('nama_user','Username','is_unique[tb_login.nama_user]');
		    $this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required');

		    if ($this->form_validation->run() == FALSE) {
		    	$flashda = $this->session->set_flashdata("nama_user", "<div class='alert alert-danger'><strong>Username Sudah digunakan!!!</strong></div>");
					$data = [
						'title' => 'Tambah Data',
						'nama' => $this->session->userdata('nama'),	
						'status' => 'baru',
						'id_user' => '',
						'nama_user' => '',
						'pass_user' => '',
						'bu_pass' => '',
						'nama_lengkap' => '',
						'level' => '',
						'isi' => 'user/form.php',
						'aktip' => '',
						'aktip2' => '',
						'aktip3' => '',
						'aktip4' => '',
						'aktip5' => '',
						'aktip6' => '',
						'aktip7' => '',
						'aktip8' => '',
						'aktip9' => '',
						'aktip10' => 'active',
						'aktip11' => '',
						'aktip12' => '',
						'aktip13' => '',
						'aktip14' => '',
						'aktip15' => '',
						'aktip16' => '',
						'aktip17' => '',
						'aktip18' => '',
						'aktip19' => ''
					];
					$this->load->view('inc/wrapper', $data, $flasda);
				}else{
				$data = array(
					'id_user' => $id_user,
					'nama_user' => $nama_user,
					'pass_user' => md5($pass_user),
					'bu_pass' => $pass_user,
					'nama' => $nama_lengkap,
					'level' => $level
					);
				$result = $this->model->Simpan('tb_login', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'user/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'user/');
				}}
			}else{
					$data = array(
					'id_user' => $id_user,
					'nama_user' => $nama_user,
					'pass_user' => md5($pass_user),
					'bu_pass' => $pass_user,
					'nama' => $nama_lengkap,
					'level' => $level
					);
				
				$result = $this->model->Update('tb_login', $data, array('id_user' => $id_user));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'user/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'user/');
				}
			}
		}else{
			echo('gagal!!!');
		}
	}

	function hapus_user($kode = 1){
		
		$result = $this->model->Hapus('tb_login', array('id_user' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'user/');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user/');
		}
	}
}