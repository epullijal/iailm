<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function index(){
		$this->load->view('login/login');
	}

	public function proseslog() {
		$data = array(
			'nama_user' => $this->input->post('nama_user', TRUE),
			'pass_user' => md5($this->input->post('pass_user', TRUE))
			);
		
		$hasil = $this->model->GetUser($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				// $sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['id_user'] = $sess->id_user;
				$sess_data['nama_user'] = $sess->nama_user;
				$sess_data['nama'] = $sess->nama;
				$sess_data['level'] = $sess->level;
				$sess_data['pass_user'] = $sess->pass_user;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='1') {
				redirect(base_url()."dashboard");
			}
			else{
				redirect(base_url()."dashboard");
			}		
		}
		else {
			echo "<script>alert('Gagal login: Cek username dan password!');history.go(-1);</script>";
		}
	}

	function logout(){
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('nama_user');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('pass_user');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('media');
	}
    // function register(){
    // 	$this->load->view('v_register');
    // }
}
