<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ref extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
		$this->load->helper('currency_format_helper');
		$this->load->library('form_validation');
	}

	public function pabrik()	{
		$data = [
			'title' => 'Data Pabrik',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_pabrik' => '',
			'nm_pabrik' => '',
			'kd_pabrik' => '',
			'data_pabrik' => $this->model->GetPabrik("order by nm_pabrik asc")->result_array(),
			'isi' => 'ref/data_pabrik.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => 'active',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function editpabrik($kode = 0)	{
		$tampung = $this->model->getPabrik("where id_pabrik = '$kode'")->result_array();
		$data = [
			'title' => 'Edit Data Pabrik',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'id_pabrik' => $tampung[0]['id_pabrik'],
			'nm_pabrik' => $tampung[0]['nm_pabrik'],
			'kd_pabrik' => $tampung[0]['kd_pabrik'],
			'data_pabrik' => $this->model->GetPabrik("where id_pabrik != '$kode' order by id_pabrik desc")->result_array(),
			'isi' => 'ref/data_pabrik.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => 'active',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savepabrik(){
		if($_POST){
			$status = $_POST['status'];
			$id_pabrik = $_POST['id_pabrik'];
			$nm_pabrik = $_POST['nm_pabrik'];
			$kd_pabrik = $_POST['kd_pabrik'];
			if($status == "baru"){
				$this->load->helper(array('form', 'url'));
				$this->load->library('form_validation');
		      $this->form_validation->set_rules(
		      'kd_pabrik','Kode Pabrik','is_unique[tb_pabrik.kd_pabrik]'
				);
		    if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata("kd_pabrik", "<div style='background:darkred;color:#ffffff;padding:5px;'>Kode sudah ada!!</div>");
					header('location:'.base_url().'ref/pabrik');
				}else{
				$data = array(
					'id_pabrik' => $id_pabrik,
					'nm_pabrik' => $nm_pabrik,
					'kd_pabrik' => $kd_pabrik
					);
				$result = $this->model->Simpan('tb_pabrik', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pabrik');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pabrik');
				}}
			}else{
				$data = array(
					'nm_pabrik' => $nm_pabrik,
					'kd_pabrik' => $kd_pabrik
					);
				$result = $this->model->Update('tb_pabrik', $data, array('id_pabrik' => $id_pabrik));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pabrik');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pabrik');
				}
			}
		}else{
			echo('GAGAL!!!');
		}
	}

	function hapuspabrik($kode = 1){
		
		$result = $this->model->Hapus('tb_pabrik', array('id_pabrik' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'ref/pabrik');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'ref/pabrik');
		}
	}



	// PEMASUKAN
	public function pemasukan()	{
		$data = [
			'title' => 'Data Pemasukan',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_pemasukan' => '',
			'nm_pemasukan' => '',
			'kd_pemasukan' => '',
			'jns' => '',
			'data_pemasukan' => $this->model->GetTransaksi("where jns = 'm' order by kd_transaksi asc")->result_array(),
			'isi' => 'ref/data_pemasukan.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => 'active',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function editpemasukan($kode = 0)	{
		$tampung = $this->model->getTransaksi("where id_transaksi = '$kode'")->result_array();
		$data = [
			'title' => 'Edit Data Pemasukan',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'id_pemasukan' => $tampung[0]['id_transaksi'],
			'nm_pemasukan' => $tampung[0]['nm_transaksi'],
			'kd_pemasukan' => $tampung[0]['kd_transaksi'],
			'data_pemasukan' => $this->model->GetTransaksi("where jns='m' and id_transaksi != '$kode' order by id_transaksi desc")->result_array(),
			'isi' => 'ref/data_pemasukan.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => 'active',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savepemasukan(){
		if($_POST){
			$status = $_POST['status'];
			$id_pemasukan = $_POST['id_pemasukan'];
			$nm_pemasukan = $_POST['nm_pemasukan'];
			$kd_pemasukan = $_POST['kd_pemasukan'];
			$jns = $_POST['jns'];
			if($status == "baru"){
				$this->load->helper(array('form', 'url'));
				$this->load->library('form_validation');
		      $this->form_validation->set_rules(
		      'kd_pemasukan','Kode Pemasukan','is_unique[tb_transaksi.kd_transaksi]'
				);
		    if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata("kd_pemasukan", "<div style='background:darkred;color:#ffffff;padding:5px;'>Kode sudah ada!!</div>");
					header('location:'.base_url().'ref/pemasukan');
				}else{
				$data = array(
					'id_transaksi' => $id_pemasukan,
					'nm_transaksi' => $nm_pemasukan,
					'kd_transaksi' => $kd_pemasukan,
					'jns' => $jns
					);
				$result = $this->model->Simpan('tb_transaksi', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pemasukan');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pemasukan');
				}}
			}else{
				$data = array(
					'nm_transaksi' => $nm_pemasukan,
					'kd_transaksi' => $kd_pemasukan
					);
				$result = $this->model->Update('tb_transaksi', $data, array('id_transaksi' => $id_pemasukan));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pemasukan');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pemasukan');
				}
			}
		}else{
			echo('GAGAL!!!');
		}
	}

	function hapuspemasukan($kode = 1){
		
		$result = $this->model->Hapus('tb_transaksi', array('id_transaksi' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'ref/pemasukan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'ref/pemasukan');
		}
	}


	// PENGELUARAN
	public function pengeluaran()	{
		$data = [
			'title' => 'Data Pengeluaran',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_pengeluaran' => '',
			'nm_pengeluaran' => '',
			'kd_pengeluaran' => '',
			'data_pengeluaran' => $this->model->getTransaksi("where jns = 'k' order by kd_transaksi asc")->result_array(),
			'isi' => 'ref/data_pengeluaran.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => 'active',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function editpengeluaran($kode = 0)	{
		$tampung = $this->model->getTransaksi("where id_transaksi = '$kode'")->result_array();
		$data = [
			'title' => 'Edit Data Pengeluaran',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'id_pengeluaran' => $tampung[0]['id_transaksi'],
			'nm_pengeluaran' => $tampung[0]['nm_transaksi'],
			'kd_pengeluaran' => $tampung[0]['kd_transaksi'],
			'data_pengeluaran' => $this->model->getTransaksi("where jns='k' and id_transaksi != '$kode' order by id_transaksi desc")->result_array(),
			'isi' => 'ref/data_pengeluaran.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => 'active',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savepengeluaran(){
		if($_POST){
			$status = $_POST['status'];
			$id_pengeluaran = $_POST['id_pengeluaran'];
			$nm_pengeluaran = $_POST['nm_pengeluaran'];
			$kd_pengeluaran = $_POST['kd_pengeluaran'];
			$jns = $_POST['jns'];
			if($status == "baru"){
				$this->load->helper(array('form', 'url'));
				$this->load->library('form_validation');
		      $this->form_validation->set_rules(
		      'kd_pengeluaran','Kode Pengeluaran','is_unique[tb_transaksi.kd_transaksi]'
				);
		    if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata("kd_pengeluaran", "<div style='background:darkred;color:#ffffff;padding:5px;'>Kode sudah ada!!</div>");
					header('location:'.base_url().'ref/pengeluaran');
				}else{
				$data = array(
					'id_transaksi' => $id_pengeluaran,
					'nm_transaksi' => $nm_pengeluaran,
					'kd_transaksi' => $kd_pengeluaran,
					'jns' => $jns
					);
				$result = $this->model->Simpan('tb_transaksi', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pengeluaran');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pengeluaran');
				}}
			}else{
				$data = array(
					'nm_transaksi' => $nm_pengeluaran,
					'kd_transaksi' => $kd_pengeluaran
					);
				$result = $this->model->Update('tb_transaksi', $data, array('id_transaksi' => $id_pengeluaran));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'ref/pengeluaran');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'ref/pengeluaran');
				}
			}
		}else{
			echo('GAGAL!!!');
		}
	}

	function hapuspengeluaran($kode = 1){
		
		$result = $this->model->Hapus('tb_transaksi', array('id_transaksi' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'ref/pengeluaran');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'ref/pengeluaran');
		}
	}

}