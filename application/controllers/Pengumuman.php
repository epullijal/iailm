<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');

class Pengumuman extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
	}

	public function index() {
		$this->load->library('pagination');
		$config['base_url'] = base_url().'pengumuman/';
		$config['total_rows'] = $this->model->jumlahPengumuman();
		$config['per_page'] = 10;
		$config['num_links'] = 2;

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data['data'] 		= $this->model->dataPengumuman($config['per_page'],$from);
		$data = [
			'title' => 'Daftar Pengumuman',
			'nama' => $this->session->userdata('nama'),	
			'data_pengumuman' => $data['data'],
			'isi' => 'pengumuman/data.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => 'active',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function form() {
		$data = [
			'title' => 'Tambah Pengumuman',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_pengumuman' => '',
			'judul_pengumuman' => '',
			'tgl_buat' => '',
			'tgl_edit' => '',
			'isi_pengumuman' => '',
			'penulis' => '',
			'status_post' => '',
			'isi' => 'pengumuman/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => 'active',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function edit_pengumuman($kode = 0){		
		$tampung = $this->model->getDataPengumuman("where id_pengumuman = '$kode'")->result_array();
		
		$data = [
			'title' => 'Edit Pengumuman',
			'nama' => $this->session->userdata('nama'),
			'status' => 'lama',
			'id_pengumuman' => $tampung[0]['id_pengumuman'],
			'judul_pengumuman' => $tampung[0]['judul_pengumuman'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'isi_pengumuman' => $tampung[0]['isi_pengumuman'],
			'penulis' => $tampung[0]['penulis'],
			'status_post' => $tampung[0]['status_post'],
			'isi' => 'pengumuman/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => 'active',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savedata(){
		if($_POST){
			$status = $_POST['status'];
			$id_pengumuman = $_POST['id_pengumuman'];
			$judul_pengumuman = $_POST['judul_pengumuman'];
			$tgl_buat = date("Y-m-d H:i:s");
			$tgl_edit = date("Y-m-d H:i:s");
			$isi_pengumuman = $_POST['isi_pengumuman'];
			$penulis = $_POST['penulis'];
			$status_post = $_POST['status_post'];
			if($status == "baru"){
				
				$data = array(
					'id_pengumuman' => $id_pengumuman,
					'judul_pengumuman' => $judul_pengumuman,
					'tgl_buat' => $tgl_buat,
					'isi_pengumuman' => $isi_pengumuman,
					'penulis' => $penulis,
					'status_post' => $status_post
					);
				$result = $this->model->Simpan('tb_pengumuman', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'pengumuman/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'pengumuman/');
				}
			}else{
				$data = array(
					'id_pengumuman' => $id_pengumuman,
					'judul_pengumuman' => $judul_pengumuman,
					'tgl_edit' => $tgl_edit,
					'isi_pengumuman' => $isi_pengumuman,
					'penulis' => $penulis,
					'status_post' => $status_post
					);
				
				$result = $this->model->Update('tb_pengumuman', $data, array('id_pengumuman' => $id_pengumuman));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'pengumuman/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'pengumuman/');
				}
			}
		}else{
			echo('gagal!!!');
		}
	}

	function hapus_pengumuman($kode = 1){
		
		$result = $this->model->Hapus('tb_pengumuman', array('id_pengumuman' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'pengumuman/');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'pengumuman/');
		}
	}
}