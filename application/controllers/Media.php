<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media extends CI_Controller {

	function index(){
		$this->load->library('pagination');
		$config['base_url'] = base_url().'media/index/';
		$config['total_rows'] = $this->model->jumlahBerita();
		$config['per_page'] = 4;
		$config['num_links'] = 2;
		$config['first_link'] = '<button class="btn btn-default btn-sm">&laquo;</button>';
		$config['last_link'] = '<button class="btn btn-default btn-sm">&raquo;</button>';
		$config['next_link'] = '<button class="btn btn-default btn-sm">&gt;</button>';
		$config['prev_link'] = '<button class="btn btn-default btn-sm">&lt;</button>';
		$config['cur_tag_open'] = '<button class="btn btn-success">';
		$config['cur_tag_close'] = '</button>';
		$config['num_tag_open'] = '<button class="btn btn-default btn-sm">';
		$config['num_tag_close'] = '</button>';
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['berita'] 		= $this->model->dataBeritaFront($config['per_page'],$from);
		$data['pengumuman'] 		= $this->model->dataPengumumanFront(3,0);

		$data['highlight_berita'] 		= $this->model->dataBeritaFront(9,0);
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'data_highlight_berita' => $data['highlight_berita'],
			'title' => 'Home',
			'data_berita' => $data['berita'],
			'data_pengumuman' => $data['pengumuman'],
			'isi' => 'front/home.php'
		];
		
		$this->load->view('front/wrapper', $data);
	}

	function detail_berita($kode = 0){
		$this->load->library('pagination');
		$config['base_url'] = base_url().'media/index/';
		$config['total_rows'] = $this->model->jumlahBerita();
		$config['per_page'] = 3;
		$config['num_links'] = 2;
		
		$data['data'] 		= $this->model->dataBeritaFront(4,0);

		$tampung = $this->model->getDataBerita("where id_berita = '$kode'")->result_array();
		
		$data['highlight_berita'] 		= $this->model->dataBeritaFront(9,0);
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'data_highlight_berita' => $data['highlight_berita'],
			'title' => 'Detail Berita',
			'id_berita' => $tampung[0]['id_berita'],
			'foto' => $tampung[0]['foto'],
			'judul_berita' => $tampung[0]['judul_berita'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'isi_berita' => $tampung[0]['isi_berita'],
			'penulis' => $tampung[0]['penulis'],
			'status_post' => $tampung[0]['status_post'],
			'data_berita' => $data['data'],
			'isi' => 'front/detail_berita.php'
		];
		$this->load->view('front/wrapper', $data);
	}

	function detail_pengumuman($kode = 0){
		$data['data'] 		= $this->model->dataBeritaFront(4,0);

		$tampung = $this->model->getDataPengumuman("where id_pengumuman = '$kode'")->result_array();
		
		$data['highlight_berita'] 		= $this->model->dataBeritaFront(9,0);
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'data_highlight_berita' => $data['highlight_berita'],
			'title' => 'Detail Berita',
			'id_pengumuman' => $tampung[0]['id_pengumuman'],
			'judul_pengumuman' => $tampung[0]['judul_pengumuman'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'isi_pengumuman' => $tampung[0]['isi_pengumuman'],
			'penulis' => $tampung[0]['penulis'],
			'status_post' => $tampung[0]['status_post'],
			'data_berita' => $data['data'],
			'isi' => 'front/detail_pengumuman.php'
		];
		$this->load->view('front/wrapper', $data);
	}
}