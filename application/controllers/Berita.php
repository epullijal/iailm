<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');

class Berita extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
	}

	public function index() {
		$this->load->library('pagination');
		$config['base_url'] = base_url().'berita/';
		$config['total_rows'] = $this->model->jumlahBerita();
		$config['per_page'] = 10;
		$config['num_links'] = 2;

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data['data'] 		= $this->model->dataBerita($config['per_page'],$from);
		$data = [
			'title' => 'Daftar Berita',
			'nama' => $this->session->userdata('nama'),	
			'data_berita' => $data['data'],
			'isi' => 'berita/data.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => 'active',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	public function form() {
		$data = [
			'title' => 'Tambah Berita',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_berita' => '',
			'foto' => '',
			'judul_berita' => '',
			'tgl_buat' => '',
			'tgl_edit' => '',
			'isi_berita' => '',
			'penulis' => '',
			'status_post' => '',
			'isi' => 'berita/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => 'active',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function edit_berita($kode = 0){		
		$tampung = $this->model->getDataBerita("where id_berita = '$kode'")->result_array();
		
		$data = [
			'title' => 'Edit Berita',
			'nama' => $this->session->userdata('nama'),
			'status' => 'lama',
			'id_berita' => $tampung[0]['id_berita'],
			'foto' => $tampung[0]['foto'],
			'judul_berita' => $tampung[0]['judul_berita'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'isi_berita' => $tampung[0]['isi_berita'],
			'penulis' => $tampung[0]['penulis'],
			'status_post' => $tampung[0]['status_post'],
			'isi' => 'berita/form.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => 'active',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function savedata(){
		if($_POST){
			if($_FILES['file_upload']['error'] == 0):
			$config = array(
				'upload_path' => './assets/upload',
				'allowed_types' => 'jpg|JPG|jpeg|png',
				'max_size' => '2048',
				
				);
		$this->load->library('upload', $config);      
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		else:
			$file_name = $this->input->post('foto');
		endif;

			$status = $_POST['status'];
			$id_berita = $_POST['id_berita'];
			$judul_berita = $_POST['judul_berita'];
			$tgl_buat = date("Y-m-d H:i:s");
			$tgl_edit = date("Y-m-d H:i:s");
			$isi_berita = $_POST['isi_berita'];
			$penulis = $_POST['penulis'];
			$status_post = $_POST['status_post'];
			if($status == "baru"){
				
				$data = array(
					'id_berita' => $id_berita,
					'foto' => $file_name,
					'judul_berita' => $judul_berita,
					'tgl_buat' => $tgl_buat,
					'isi_berita' => $isi_berita,
					'penulis' => $penulis,
					'status_post' => $status_post
					);
				$result = $this->model->Simpan('tb_berita', $data);
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'berita/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'berita/');
				}
			}else{
				$data = array(
					'id_berita' => $id_berita,
					'foto' => $file_name,
					'judul_berita' => $judul_berita,
					'tgl_edit' => $tgl_edit,
					'isi_berita' => $isi_berita,
					'penulis' => $penulis,
					'status_post' => $status_post
					);
				
				$result = $this->model->Update('tb_berita', $data, array('id_berita' => $id_berita));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'berita/');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'berita/');
				}
			}
		}else{
			echo('gagal!!!');
		}
	}

	function hapus_berita($kode = 1){
		
		$result = $this->model->Hapus('tb_berita', array('id_berita' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'berita/');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'berita/');
		}
	}
}