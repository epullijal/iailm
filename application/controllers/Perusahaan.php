<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	public function __construct()	{
		parent::__construct();
		$this->load->library('upload');
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
	}

	public function index()	{
		$data = [
			'title' => 'Data Lembaga',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'baru',
			'id_perusahaan' => '',
			'nama_perusahaan' => '',
			'telp' => '',
			'email' => '',
			'alamat' => '',
			'foto' => '',
			'data_perusahaan' => $this->model->getPerusahaan()->result_array(),
			'isi' => 'ref/data_perusahaan.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => 'active',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];

		$this->load->view('inc/wrapper', $data);
	}

	function editPerusahaan($kode = 0){		
		$tampung = $this->model->getPerusahaan("where id_perusahaan = '$kode'")->result_array();
		$data = [
			'title' => 'Edit Data Lembaga',
			'nama' => $this->session->userdata('nama'),	
			'status' => 'lama',
			'nama_perusahaan' => $tampung[0]['nama_perusahaan'],
			'telp' => $tampung[0]['telp'],
			'email' => $tampung[0]['email'],
			'alamat' => $tampung[0]['alamat'],
			'foto' => $tampung[0]['foto'],
			'id_perusahaan' => $tampung[0]['id_perusahaan'],
			'data_perusahaan' => $this->model->getPerusahaan("where id_perusahaan != '$kode' order by id_perusahaan desc")->result_array(),
			'isi' => 'ref/data_perusahaan.php',
			'aktip' => '',
			'aktip2' => 'treeview active',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => 'active',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
			];
		$this->load->view('inc/wrapper', $data);
	}

	function tambah_data(){
		$config = array(
		'upload_path' 	=> './assets/',
		'allowed_types' => 'gif|jpg|JPG|png',
		'max_size' 		=> '2048',
		);

		$this->load->library('upload', $config);	
		$this->upload->do_upload('userfile');
		$upload_data = $this->upload->data();

		$id_perusahaan = $_POST['id_perusahaan'];
		$nama_perusahaan = $_POST['nama_perusahaan'];
		$file_name = $upload_data['file_name'];
		$telp = $_POST['telp'];
		$email = $_POST['email'];
		$alamat = $_POST['alamat'];
		
		$data = array(
			'id_perusahaan' => $id_perusahaan,
			'nama_perusahaan' => $nama_perusahaan,
			'foto' => $file_name,
			'telp' => $telp,
			'email' => $email,
			'alamat' => $alamat
			);
		$result = $this->model->Simpan('tb_perusahaan', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'perusahaan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'perusahaan');
		}
	}

	function update_data(){
		if($_FILES['file_upload']['error'] == 0):
			$config = array(
				'upload_path' => './assets/',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',
				
				);
		$this->load->library('upload', $config);      
		$this->upload->do_upload('userfile');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		else:
			$file_name = $this->input->post('foto');
		endif;
		
		$data = array(
			'id_perusahaan' => $this->input->post('id_perusahaan'),
			'nama_perusahaan' => $this->input->post('nama_perusahaan'),
			'foto' => $file_name,
			'telp' => $this->input->post('telp'),
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('alamat')
		);
			$result = $this->model->Update('tb_perusahaan', $data, array('id_perusahaan' => $id_perusahaan));
			if($result == 1){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'perusahaan');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'perusahaan');
			}
	}

}