<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function index() {
		$this->load->library('pagination');
		$config['base_url'] = base_url().'front/index/';
		$config['total_rows'] = $this->model->jumlahBerita();
		$config['per_page'] = 4;
		$config['num_links'] = 2;
		$config['first_link'] = '<button class="btn btn-default btn-sm">&laquo;</button>';
		$config['last_link'] = '<button class="btn btn-default btn-sm">&raquo;</button>';
		$config['next_link'] = '<button class="btn btn-default btn-sm">&gt;</button>';
		$config['prev_link'] = '<button class="btn btn-default btn-sm">&lt;</button>';
		$config['cur_tag_open'] = '<button class="btn btn-success">';
		$config['cur_tag_close'] = '</button>';
		$config['num_tag_open'] = '<button class="btn btn-default btn-sm">';
		$config['num_tag_close'] = '</button>';
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['berita'] 		= $this->model->dataBeritaFront($config['per_page'],$from);
		$data['pengumuman'] 		= $this->model->dataPengumumanFront(3,0);

		$data['highlight_berita'] 		= $this->model->dataBeritaFront(9,0);
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'data_highlight_berita' => $data['highlight_berita'],
			'title' => 'Home',
			'data_berita' => $data['berita'],
			'data_pengumuman' => $data['pengumuman'],
			'isi' => 'front/home.php'
		];
		
		$this->load->view('front/wrapper', $data);
	}

	public function pimpinan($kode=0)	{
		$data['data'] = $this->model->dataBeritaFront(4,0);
		$tampung = $this->model->getDataProfil("where id = '$kode'")->result_array();
		$data['highlight_berita'] 		= $this->model->dataBeritaFront(9,0);
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'data_highlight_berita' => $data['highlight_berita'],
			'title' => 'Detail Berita',
			'id_berita' => $tampung[0]['id_berita'],
			'foto' => $tampung[0]['foto'],
			'judul_berita' => $tampung[0]['judul_berita'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'isi_berita' => $tampung[0]['isi_berita'],
			'penulis' => $tampung[0]['penulis'],
			'status_post' => $tampung[0]['status_post'],
			'data_berita' => $data['data'],
			'isi' => 'front/detail_berita.php'
		];
		$this->load->view('front/wrapper', $data);
	}



	// DATA ADMIN
	public function table_pimpinan()	{
		$data = [
			'title' => 'Data Pimpinan',
			'nama' => $this->session->userdata('nama'),	
			'data_pimpinan' => $this->model->getProfil("where id = 1")->result_array(),
			'isi' => 'profil/data_pimpinan.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => 'treeview active',
			'aktip6' => 'active',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];

		$this->load->view('inc/wrapper', $data);
	}

	function edit_pimpinan($kode = 0){		
		$tampung = $this->model->getProfil("where id = 1")->result_array();
		
		$data = [
			'title' => 'Edit Data Pimpinan',
			'nama' => $this->session->userdata('nama'),
			'status' => 'lama',
			'id' => $tampung[0]['id'],
			'judul' => $tampung[0]['judul'],
			'tgl_buat' => $tampung[0]['tgl_buat'],
			'tgl_edit' => $tampung[0]['tgl_edit'],
			'deskripsi' => $tampung[0]['deskripsi'],
			'penulis' => $tampung[0]['penulis'],
			'isi' => 'profil/form_pimpinan.php',
			'aktip' => '',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => 'treeview active',
			'aktip6' => 'active',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		$this->load->view('inc/wrapper', $data);
	}

	function save_pimpinan(){
		if($_POST){
			$status = $_POST['status'];
			$id = $_POST['id'];
			$judul = $_POST['judul'];
			$tgl_buat = date("Y-m-d H:i:s");
			$tgl_edit = date("Y-m-d H:i:s");
			$deskripsi = $_POST['deskripsi'];
			$penulis = $_POST['penulis'];
			if($status == "lama"){
				$data = array(
					'id' => $id,
					'judul' => $judul,
					'tgl_edit' => $tgl_buat,
					'deskripsi' => $deskripsi,
					'penulis' => $penulis
					);
				
				$result = $this->model->Update('tb_profil', $data, array('id' => $id));
				if($result == 1){
					$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL dilakukan</strong></div>");
					header('location:'.base_url().'profil/table_pimpinan');
				}else{
					$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
					header('location:'.base_url().'profil/table_pimpinan');
				}
			}
		}else{
			echo('gagal!!!');
		}
	}

}