<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model');
		if ($this->session->userdata('nama_user')=="") {
			redirect('login');
		}
		$this->load->helper('currency_format_helper');
	}

	public function index()	{
		$dataperusahaan = $this->model->getPerusahaan()->result_array();
		$data = [
		'nama_per' => $dataperusahaan[0]['nama_perusahaan'],
			'logo_per' => $dataperusahaan[0]['foto'],
			'telp_per' => $dataperusahaan[0]['telp'],
			'email_per' => $dataperusahaan[0]['email'],
			'alamat_per' => $dataperusahaan[0]['alamat'],
			'title' => 'Dashboard',
			'nama' => $this->session->userdata('nama'),
			'isi' => 'index.php',
			'aktip' => 'active',
			'aktip2' => '',
			'aktip3' => '',
			'aktip4' => '',
			'aktip5' => '',
			'aktip6' => '',
			'aktip7' => '',
			'aktip8' => '',
			'aktip9' => '',
			'aktip10' => '',
			'aktip11' => '',
			'aktip12' => '',
			'aktip13' => '',
			'aktip14' => '',
			'aktip15' => '',
			'aktip16' => '',
			'aktip17' => '',
			'aktip18' => '',
			'aktip19' => ''
		];
		
		$this->load->view('inc/wrapper', $data);
	}
}

