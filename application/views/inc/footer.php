</div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <!-- <b>Version</b> 2.0 -->
        </div>
        <strong>iailm suryalaya &copy; 2017 <a href="#"></a></strong>
      </footer>
    </div><!-- ./wrapper -->
        <!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="<?php echo base_url(); ?>assets/dist/js/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- datepicker -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>

    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>

    <!-- membuat efek animasi -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function() {
        $(".select2").select2();
        $('.datepicker').datepicker({
          todayHighlight: true,
          autoclose: true
        });
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
      //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });

      function TotalPotong(){
        var a3 = document.getElementById('totUpah').value;
        var b3 = document.getElementById('hasil_ptg').value;
        var c3 = document.getElementById('listrik_ptg').value;
        var d3 = document.getElementById('lain_ptg').value;
        var e3 = document.getElementById('uptimbang').value;
        var result = (parseInt(a3)+parseInt(e3)) * parseInt(b3) + parseInt(c3) + parseInt(d3);
        if (!isNaN(result)) {
          document.getElementById('costTotal').value = result;
        }
      }

      function TotalKg(){
        var a4 = $(".costTotal").val();
        var b4 = $(".hasil_ptg").val();
        c4 = a4 / b4;
        $(".costKg").val(c4);
      }

      function jmlSisa(){
        var a5 = $(".jml_kirim").val();
        var b5 = $(".jml_susut").val();
        c5 = a5 - b5;
        $(".jml_sisa").val(c5);
      }

      function harga_total(){
        var a6 = $(".jml_sisa").val();
        var b6 = $(".harga_jual").val();
        c6 = a6 * b6;
        $(".hargaTotal").val(c6);
      }

      function sisaUang(){
        var a7 = $(".hargaTotal").val();
        var b7 = $(".jmlUang").val();
        c7 = a7 - b7;
        $(".sisa_uang").val(c7);
      }

      function costKg(){
        var a8 = document.getElementById('total_upahc').value;
        var b8 = document.getElementById('listrik_ctk').value;
        var result = parseInt(a8) + parseInt(b8);
        if (!isNaN(result)) {
          document.getElementById('hasil_costKg').value = result;
        }
      }

      function sisa_barang(){
        var a9 = $(".jmlBarang").val();
        var b9 = $(".jmlSusut").val();
        c9 = a9 - b9;
        $(".sisaBarang").val(c9);
      }

      function jml_keluar_armada(){
        var a10 = $(".solarArmada").val();
        var b10 = $(".upSopirArmada").val();
        var c10 = $(".upBongkarArmada").val();
        var d10 = $(".lainArmada").val();
        e10 = parseInt(a10)+parseInt(b10)+parseInt(c10)+parseInt(d10);
        $(".jmlKeluarArmada").val(e10);
      }

      function hitungArmada(){
        var a11 = $(".jmlKeluarArmada").val();
        var b11 = $(".jmlMasukArmada").val();
        c11 = parseInt(b11)-parseInt(a11);
        $(".labaRugiArmada").val(c11);
      }

      function checkForm(form)  {
        if(form.nama_user.value == "") {
          alert("Error: Username tidak boleh kosong!");
          form.nama_user.focus();
          return false;
        }
        re = /^\w+$/;
        if(!re.test(form.nama_user.value)) {
          alert("Error: Username hanya boleh berisi huruf, angka dan underscore!");
          form.nama_user.focus();
          return false;
        }

        if(form.pass_user.value != "" && form.pass_user.value == form.pass_user2.value) {
          if(form.pass_user.value.length < 6) {
            alert("Error: Password harus berisi paling sedikit 6 karakter!");
            form.pass_user.focus();
            return false;
          }
          if(form.pass_user.value == form.nama_user.value) {
            alert("Error: Password tidak boleh sama dengan Username!");
            form.pass_user.focus();
            return false;
          }
          re = /[0-9]/;
          if(!re.test(form.pass_user.value)) {
            alert("Error: password harus berisi paling tidak satu angka (0-9)!");
            form.pass_user.focus();
            return false;
          }
        } else {
          alert("Error: Pastikan anda mengisikan password yang benar!");
          form.pass_user2.focus();
          return false;
        }
      }
    </script>

    <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
    <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
    <script type="text/javascript">
    function printDiv(elementId) {
     var a = document.getElementById('printing-css').value;
     var b = document.getElementById(elementId).innerHTML;
     window.frames["print_frame"].document.title = document.title;
     window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
     window.frames["print_frame"].window.focus();
     window.frames["print_frame"].window.print();
    }
    </script>

    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
      $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".textareaberita").wysihtml5();
      });
    </script>
  </body>
</html>
    

    
    