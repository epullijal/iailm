<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo base_url(); ?>dashboard" class="logo"><b>IAILM</b> Suryalaya</a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs"><?php echo $nama; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header" style="height: 120px;">
              <p>
                <?php echo $nama; ?><br><br>
                <small><?php if($this->session->userdata('level')==1){echo "<span class='alert alert-danger'>Admin</span>";}elseif($this->session->userdata('level')==2){echo "<span class='alert alert-warning'>Admin Cabang</span>";}elseif($this->session->userdata('level')==1){echo "<span class='alert alert-success'>Admin Pusat</span>";}?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a onclick="alert('Anda akan logout aplikasi!');" href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>