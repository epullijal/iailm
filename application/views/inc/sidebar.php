<aside class="main-sidebar">
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/dist/img/user.png" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p style="font-size:12px;"><?php echo $nama; ?></p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- ADMIN CABANG -->
      <li class="header">Menu Admin</li>
      <li class="<?= $aktip ?>">
        <a href="<?php echo base_url(); ?>dashboard">
          <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
      </li>

      <li class="<?= $aktip2 ?>" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Refrensi</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $aktip15 ?>"><a href="<?php echo base_url(); ?>perusahaan"><i class="fa fa-building"></i> Data Lembaga</a></li>
          
        </ul>
      </li>

      <li class="<?= $aktip5 ?>" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
        <a href="#">
          <i class="fa fa-cube"></i> <span>Profil</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $aktip6 ?>"><a href="<?php echo base_url(); ?>profil/table_pimpinan"><i class="fa fa-user"></i> Pimpinan</a></li>
          
        </ul>
      </li>

      <li class="<?= $aktip3 ?>">
        <a href="<?php echo base_url(); ?>berita/">
          <i class="fa fa-file"></i> <span>Berita</span>
        </a>
      </li>

      <li class="<?= $aktip4 ?>">
        <a href="<?php echo base_url(); ?>pengumuman/">
          <i class="fa fa-bell"></i> <span>Pengumuman</span>
        </a>
      </li>

      <li class="<?= $aktip10 ?>" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
        <a href="<?php echo base_url(); ?>user/">
          <i class="fa fa-user"></i> <span>User</span>
        </a>
      </li>

      <li class="header"></li>
      <!-- <li><a href="#"><i class="fa fa-circle-o text-danger"></i> Important</a></li>
      <li><a href="#"><i class="fa fa-circle-o text-warning"></i> Warning</a></li>
      <li><a href="#"><i class="fa fa-circle-o text-info"></i> Information</a></li> -->
    </ul>
  </section>
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <b><?= $title ?></b>
    </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> -->
  </section>