<!-- === BEGIN CONTENT === -->
<div id="content">
<div class="container no-padding menu-atas">
  <div class="row">
    <!-- Carousel Slideshow -->
    <div id="carousel-example" class="carousel slide" data-ride="carousel">
      <!-- Carousel Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example" data-slide-to="1"></li>
      </ol>
      <div class="clearfix"></div>
      <!-- End Carousel Indicators -->
      <!-- Carousel Images -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?=base_url();?>assets/front/img/slideshow/slide1.jpg">
        </div>
        <div class="item">
          <img src="<?=base_url();?>assets/front/img/slideshow/slide2.jpg">
        </div>
      </div>
      <!-- End Carousel Images -->
      <!-- Carousel Controls -->
      <a class="left carousel-control" href="#carousel-example" data-slide="prev">
      <i style="padding-top:8px;" class="fa fa-chevron-left fa-2x"></i>
      </a>
      <a class="right carousel-control" href="#carousel-example" data-slide="next">
        <i style="padding-top:8px;" class="fa fa-chevron-right fa-2x"></i>
      </a>
      <!-- End Carousel Controls -->
    </div>
    <!-- End Carousel Slideshow -->
  </div>
</div>

<div class="container">
    <div class="row margin-vert-30 judul-home">
        <div class="col-md-12">
            <h2>Berita Terbaru</h2>
        </div>
    </div>
    <div class="row animate fadeInDown">
        <div class="col-md-12 portfolio-group no-padding">
          <div class="row">
            <!-- Portfolio Item -->
            <?php foreach($data_berita as $row) { ?>
              <div class="col-md-3 portfolio-item margin-bottom-40 code">
              <div>              
                  <figure>
                    <img src="<?=base_url();?>assets/upload/<?php echo $row->foto; ?>" alt="<?php echo $row->foto; ?>">
                    <div class="">
                      <a href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>">
                        <h3 style="margin-top:10px;margin-bottom:-20px;"><?php echo $row->judul_berita; ?></h3>
                      </a>
                    </div>

                    <p style="background:#f0f0f0;border-left: 5px solid #dadada;padding-left:5px;">
                        <i class="fa fa-pencil"></i>
                        <a href="#"> <?php echo $row->penulis; ?></a>&nbsp;
                        <i class="fa fa-calendar"> </i>&nbsp;
                        <?php echo tgl_indo($row->tgl_buat); ?>
                    </p>
                    
                    <figcaption>
                      <span style="color:#212121; "><?php echo substr($row->isi_berita, 0,145) ?>.... <a href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>">Selengkapnya...</a></span>
                    </figcaption>
                  </figure>
                </div>
              </div>
            <?php } ?>
            <!-- End Portfolio Item -->
            </div>
        </div>

    </div>
        <?php echo $this->pagination->create_links(); ?>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-8">
        <h2 class="fa-bullhorn judul-home"> Pengumunan</h2>
      <div class="blog-item-footer">
      <br>
      <?php foreach($data_pengumuman as $row2) { ?>
      <blockquote class="primary animate fadeInLeftBig">
        <h4><strong><a href="<?php echo base_url();?>front/detail_pengumuman/<?php echo $row2->id_pengumuman; ?>"> <?php echo $row2->judul_pengumuman; ?></a></strong></h4>
          <p style="text-align: justify;">
            <?php echo substr($row2->isi_pengumuman, 0,70); ?>... <a href="<?php echo base_url();?>front/detail_pengumuman/<?php echo $row2->id_pengumuman; ?>">Selengkapnya...</a>
          </p>
          <small>
              <i class="fa fa-clock-o"> </i> <?php echo tgl_indo($row2->tgl_buat)," ". substr($row2->tgl_buat, 11) ?>
          </small>
      </blockquote>
      <?php } ?>
    </div>
    </div>
    
  </div>
</div>
<br>
</div>
                  
              