<!-- === BEGIN CONTENT === -->
<div id="content">
  <div class="container">
    <div class="row margin-vert-30">
      <!-- Main Column -->
      <div class="col-md-9">
        <!-- Main Content -->

        <a href="<?php echo base_url();?>" title="kembali" class="btn btn-default btn-sm"> <i class="fa fa-undo"> </i> </a>
        <a href="javascript:printDiv('print-berita');" class="btn btn-default btn-sm" title="print / simpan pdf"> <i class="fa fa-print"> </i> </a>
        <br>
        <br>
        <div id="print-berita" class="animate fadeInRight">
          <h2><?php echo $judul_berita; ?></h2>
          <br>
          <img style="float:left;margin-right: 20px;margin-bottom: 10px;" class="thumbnail" height="200" src="<?php echo base_url();?>assets/upload/<?php echo $foto; ?>">
          <p style="text-align: justify;">
            <?php echo $isi_berita; ?>
          </p>
          <p style="font-size: 12px;">
            Oleh: <?php echo $penulis; ?>
          </p>
        </div>
        <!-- End Main Content -->
      </div>
      <!-- End Main Column -->
      <!-- Side Column -->
      <div class="col-md-3">
        <!-- Recent Posts -->
        <div class="panel panel-border-green">
          <div class="panel-heading">
          <h3 class="panel-title">Berita Terakhir</h3>
          </div>
          <div class="panel-body">
            <ul class="posts-list margin-top-10">
            <?php foreach($data_berita as $row) { ?>
              <li>
                <div class="recent-post">
                  <a class="pull-left" href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>">
                    <img width="60" src="<?=base_url();?>assets/upload/<?php echo $row->foto; ?>" alt="<?php echo $row->foto; ?>">
                  </a>
                  <a style="" href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>" class="posts-list-title"><?php echo substr($row->judul_berita, 0,20); ?> ...
                  </a><br>
                  <span class="recent-post-date">
                    <?php echo tgl_indo($row->tgl_edit);?>
                  </span>
                </div>
                <div class="clearfix"></div>
              </li>
            <?php } ?>
            </ul>
          </div>
        </div>
        <!-- End recent Posts -->
      </div>
      <!-- End Side Column -->
    </div>
  </div>
</div>