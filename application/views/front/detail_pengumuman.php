<!-- === BEGIN CONTENT === -->
<div id="content">
  <div class="container">
    <div class="row margin-vert-30">
      <!-- Main Column -->
      <div class="col-md-9">
        <!-- Main Content -->

        <a href="<?php echo base_url();?>" title="kembali" class="btn btn-default btn-sm"> <i class="fa fa-undo"> </i> </a>
        <a href="javascript:printDiv('print-pengumuman');" class="btn btn-default btn-sm" title="print / simpan pdf"> <i class="fa fa-print"> </i> </a>
        <br>
        <br>
        <div id="print-pengumuman">
          <h2><?php echo $judul_pengumuman; ?></h2>
          <br>
          <p style="text-align: justify;">
            <?php echo $isi_pengumuman; ?>
          </p>
        </div>
        <!-- End Main Content -->
      </div>
      <!-- End Main Column -->
      <!-- Side Column -->
      <div class="col-md-3">
        <!-- Recent Posts -->
        <div class="panel panel-success">
          <div class="panel-heading">
          <h3 class="panel-title">Berita Terakhir</h3>
          </div>
          <div class="panel-body">
            <ul class="posts-list margin-top-10">
            <?php foreach($data_berita as $row) { ?>
              <li>
                <div class="recent-post">
                  <a href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>">
                    <img width="40" src="<?=base_url();?>assets/upload/<?php echo $row->foto; ?>" alt="<?php echo $row->foto; ?>">
                  </a>
                  <a href="<?php echo base_url();?>front/detail_berita/<?php echo $row->id_berita; ?>" class="posts-list-title"><?php echo substr($row->judul_berita, 0,20); ?> ...</a>
                  <br>
                  <span class="recent-post-date">
                    <?php echo tgl_indo($row->tgl_edit);?>
                  </span>
                </div>
                <div class="clearfix"></div>
              </li>
            <?php } ?>
            </ul>
          </div>
        </div>
        <!-- End recent Posts -->
      </div>
      <!-- End Side Column -->
    </div>
  </div>
</div>