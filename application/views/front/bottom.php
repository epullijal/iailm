  </div>
</div>
<!-- === END CONTENT === -->
<!-- === BEGIN FOOTER === -->
<div id="base">
  <div class="container padding-vert-30 margin-top-40 animate fadeInUpBig" style="border:1px solid #27c00a">
    <div class="row">
      <!-- Sample Menu -->
      <div class="col-md-3 margin-bottom-20">
        <h3 class="margin-bottom-10">Menu</h3>
        <ul class="menu">
          <li>
            <a class="fa-tasks" href="#">Placerat facer possim</a>
          </li>
          <li>
            <a class="fa-users" href="#">Quam nunc putamus</a>
          </li>
          <li>
            <a class="fa-signal" href="#">Velit esse molestie</a>
          </li>
          <li>
            <a class="fa-coffee" href="#">Nam liber tempor</a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- End Sample Menu -->
      <!-- Contact Details -->
      <div class="col-md-3 margin-bottom-20">
        <h3 class="margin-bottom-10">Kontak</h3>
        <p>
          <span class="fa-phone"></span><?php echo $telp_per; ?>
          <br>
          <span class="fa-envelope"></span>
          <a href="mailto:iailm.suryalaya@gmail.com"><?php echo $email_per; ?></a>
          <br>
          <span class="fa-globe"></span>
          <a target="_blank" href="http://www.iailm.ac.id">www.iailm.ac.id</a>
        </p>
        <p><?php echo $alamat_per; ?></p>
        </div>
        <!-- End Contact Details -->
        <!-- Thumbs Gallery -->
        <div class="col-md-3 margin-bottom-20">
          <h3 class="margin-bottom-10">Highlights Berita</h3>
          <div class="thumbs-gallery">
            <?php foreach($data_highlight_berita as $row) { ?>
            <a class="thumbBox" rel="lightbox-thumbs" href="<?=base_url();?>assets/upload/<?php echo $row->foto; ?>">
              <img width="68" height="68" src="<?=base_url();?>assets/upload/<?php echo $row->foto; ?>" alt="<?php echo $row->foto; ?>">
              <i></i>
            </a>
            <?php } ?>
            
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- End Thumbs Gallery -->
        <!-- Disclaimer -->
        <div class="col-md-3 margin-bottom-20">
          <h3 class="margin-bottom-10">Disclaimer</h3>
          <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit
            augue duis dolore te feugait nulla facilisi.</p>
            <div class="clearfix"></div>
          </div>
          <!-- End Disclaimer -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <!-- Footer Menu -->
    <div id="footer">
      <div class="container">
        <div class="row">
          <div id="copyright" class="col-md-4">
            <p style="color:#27c00a;">&copy; 2016 iailmsuryalaya</p>
          </div>
          <div id="footermenu" class="col-md-8">
            <ul class="list-unstyled list-inline pull-right">
              <li>
                <a href="#" target="_blank">Sample Link</a>
              </li>
              <li>
                <a href="#" target="_blank">Sample Link</a>
              </li>
              <li>
                <a href="#" target="_blank">Sample Link</a>
              </li>
              <li>
                <a href="#" target="_blank">Sample Link</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Footer Menu -->
    <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
    <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
    <script type="text/javascript">
    function printDiv(elementId) {
     var a = document.getElementById('printing-css').value;
     var b = document.getElementById(elementId).innerHTML;
     window.frames["print_frame"].document.title = document.title;
     window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
     window.frames["print_frame"].window.focus();
     window.frames["print_frame"].window.print();
    }
    </script>
    <!-- JS -->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/scripts.js"></script>
    <!-- Isotope - Portfolio Sorting -->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/jquery.isotope.js" type="text/javascript"></script>
    <!-- Mobile Menu - Slicknav -->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/jquery.slicknav.js" type="text/javascript"></script>
    <!-- Animate on Scroll-->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/jquery.visible.js" charset="utf-8"></script>
    <!-- Sticky Div -->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/jquery.sticky.js" charset="utf-8"></script>
    <!-- Slimbox2-->
    <script type="text/javascript" src="<?=base_url();?>assets/front/js/slimbox2.js" charset="utf-8"></script>
    <!-- Modernizr -->
    <script src="<?=base_url();?>assets/front/js/modernizr.custom.js" type="text/javascript"></script>
    <!-- End JS -->
  </body>
  </html>
<!-- === END FOOTER === -->