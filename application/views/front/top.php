<?php
include 'assets/tanggal_indo.php';
?>
<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <!-- Title -->
  <title><?php echo $title; ?> | iailm suryalaya</title>
  <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo-iailm-min.png">
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <!-- Favicon -->
  <link href="favicon.ico" rel="shortcut icon">
  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/bootstrap.css" rel="stylesheet">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/animate.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/nexus.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/custom.css" rel="stylesheet">
  <!-- Google Fonts-->
  <link href="http://fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet" type="text/css">
</head>
<body>
  <div id="body_bg">
    <div id="pre_header" class="container">
      <div class="row margin-top-10 visible-lg">
        <div class="col-md-6">
          <p>
            <strong>Phone:</strong>&nbsp;<?php echo $telp_per; ?></p>
          </div>
          <div class="col-md-6 text-right">
            <p>
              <strong>Email:</strong><?php echo $email_per; ?></p>
            </div>
          </div>
        </div>
        <div class="primary-container-group">
          <!-- Background -->
          <div class="primary-container-background">
            <div class="primary-container"></div>
            <div class="clearfix"></div>
          </div>
          <!--End Background -->
          <div class="primary-container">
            <div id="header" class="container">
              <div class="row">
                <!-- Logo -->
                <div class="logo">
                  <a href="<?php echo base_url();?>" title="">
                    <img height="50" src="<?=base_url();?>assets/images/logo-depan.png" alt="Logo" />
                  </a>
                </div>
                <!-- End Logo -->
                <ul class="social-icons circle color pull-right hidden-xs">
                  <li class="social-rss">
                    <a href="#" target="_blank" title="RSS"></a>
                  </li>
                  <li class="social-twitter">
                    <a href="#" target="_blank" title="Twitter"></a>
                  </li>
                  <li class="social-facebook">
                    <a href="https://www.facebook.com/iailm.suryalaya" target="_blank" title="Facebook"></a>
                  </li>
                  <li class="social-googleplus">
                    <a href="#" target="_blank" title="GooglePlus"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- Top Menu -->
            <div id="hornav" class="container no-padding menu-atas">
              <div class="row">
                <div class="col-md-12 no-padding">
                  <div class="pull-right visible-lg">
                    <ul id="hornavmenu" class="nav navbar-nav">
                      <li>
                        <a href="<?= base_url(); ?>" class="fa-home">Home</a>
                      </li>

                      <li>
                        <span class="fa-file">Profil</span>
                        <ul>
                          <li>
                            <a href="<?= base_url(); ?>profil/pimpinan">Pimpinan</a>
                          </li>
                          <li>
                            <a href="pages-about-us.html">Sekilas</a>
                          </li>
                          <li>
                            <a href="pages-about-us.html">Sejarah</a>
                          </li>
                          <li>
                            <a href="pages-services.html">Visi, Misi & Tujuan</a>
                          </li>
                          <li>
                            <a href="pages-faq.html">Renstra</a>
                          </li>
                          <li>
                            <a href="pages-about-me.html">Landasan</a>
                          </li>
                        </ul>
                      </li>

                      <li>
                        <span class="fa-book">Fakultas</span>
                        <ul>
                          <li class="parent">
                            <span>Dakwah</span>
                            <ul>
                              <li>
                                <a href="features-typo-basic.html">Akhlak Tasawuf</a>
                              </li>
                              <li>
                                <a href="features-typo-blockquotes.html">Komunikasi & Penyiaran Islam</a>
                              </li>
                            </ul>
                          </li>
                          <li class="parent">
                            <span>Syari`ah</span>
                            <ul>
                              <li>
                                <a href="features-labels.html">Ekonomi Syari`ah</a>
                              </li>
                              <li>
                                <a href="features-progress-bars.html">Mu`amalah</a>
                              </li>
                            </ul>
                          </li>
                          <li class="parent">
                            <span>Tarbiyah</span>
                            <ul>
                              <li>
                                <a href="features-icons.html">Pend. Guru RA</a>
                              </li>
                              <li>
                                <a href="features-icons-social.html">Pend. Guru MI</a>
                              </li>
                              <li>
                                <a href="features-icons-font-awesome.html">Pend. Agama Islam</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                      
                      <li>
                        <span class="fa-th">Aplikasi</span>
                        <ul>
                          <li>
                            <a href="portfolio-2-column.html">SIAK IAILM</a>
                          </li>
                          <li>
                            <a href="portfolio-3-column.html">Perpustakaan IAILM</a>
                          </li>
                        </ul>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Top Menu -->
            <!-- === END HEADER === -->
            
