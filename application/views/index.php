<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>0</h3>
          <p><b>BERITA</b></p>
        </div>
        <div class="icon">
          <i class="fa fa-file-o"></i>
        </div>
        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-right"></i></a>
      </div>
    </div><!-- ./col -->
    
    <div class="col-lg-3 col-xs-6" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>0</h3>
          <p><b>PROGRAM STUDI</b></p>
        </div>
        <div class="icon">
          <i class="fa fa-building"></i>
        </div>
        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-sm-3">
    <div class="box">
    <div class="box-header"></div>
      <div class="box-body" align="center">
        <img height="200" src="<?php echo base_url();?>assets/images/<?php echo $logo_per; ?>">
      </div>
    <div class="box-header"></div>
    </div>
  </div>
  </div><!-- /.row -->
  <!-- Main row -->
 

</section><!-- /.content -->