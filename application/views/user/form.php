	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-danger">
					<div class="box-header with-border">
						<i class="fa fa-users"> </i>
						<h3 class="box-title"> Form User</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div><!-- /.box-tools -->
					</div> <!-- box-header -->

					<div class="box-body">
						<form class="form-horizontal" role="form" action="<?php echo base_url(); ?>user/savedata" method="post" onsubmit="return checkForm(this);">
							<input type="hidden" name="id_user" value="<?php echo $id_user; ?>"/>
							<input type="hidden" name="status" value="<?php echo $status; ?>"/>

							<div class="form-group">
								<label class="control-label col-md-2"> Nama Lengkap</label>
								<div class="col-md-4">
									<input type="text" name="nama_lengkap" class="form-control" value="<?php echo $nama_lengkap; ?><?php echo set_value('nama_lengkap');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"> Username</label>
								<div class="col-md-3">
									<input type="text" name="nama_user" class="form-control" value="<?php echo set_value('nama_user');?><?php echo $nama_user; ?>">
								</div>

        				<span id="pesan-error-flash"><?php echo $this->session->flashdata('nama_user'); ?></span>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"> Password</label>
								<div class="col-md-3">
									<input type="password" name="pass_user" class="form-control" value="<?php echo $pass_user; ?>">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"> Konfirmasi Password</label>
								<div class="col-md-3">
									<input type="text" name="pass_user2" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2"> Hak Akses</label>
								<div class="col-md-3">
									<select name="level" class="form-control">
										<option>-Hak Akses-</option>
										<option value="1" <?php echo ($level==1) ? 'selected="selected"' : ''; ?>> Super Admin</option>
										<option value="2" <?php echo ($level==2) ? 'selected="selected"' : ''; ?>> Admin</option>
									</select>
								</div>
							</div>

							<hr>
							<div class="form-group">
								<div class="col-md-10">
									<button type="submit" class="btn btn-primary btn-flat">Simpan</button>
									<button type="reset" class="btn btn-default btn-flat">Reset</button>
									<a href="<?php echo base_url(); ?>user/" class="btn btn-warning btn-flat">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div> <!-- box -->
			</div> <!-- col -->
		</div> <!-- row -->
	</section>