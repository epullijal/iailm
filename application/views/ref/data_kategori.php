  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

                <!-- Left col -->
    <section class="col-md-3" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
      <!-- Chat box -->
      <div class="box">
        <div class="box-header">
          <i class="fa fa-edit"></i>
          <h3 class="box-title">Form Jenis Bahan Baku</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <!-- chat item -->
          <div class="item">
            <form role="form" action="<?php echo base_url(); ?>kategori/savedata" method="post">
              <input type="hidden" name="id_kat" value="<?php echo $id_kat; ?>" />
              <input type="hidden" name="status" value="<?php echo $status; ?>" />

              <div class="form-group">
                <label for="namalengkap">Nama Jenis</label>
                  <input type="text" class="form-control" value="<?php echo $kategori; ?>" id="" name="kategori" placeholder="Isi Nama kategori" required>
                  <!-- <input type="text" class="form-control" value="<?php echo $kategori; ?>" id="" name="kategori" placeholder="Isikan kategori" required> -->
              </div>

              <div class="form-group">
                <label for="namalengkap"> Harga per meter</label>
                <input type="number" class="form-control" name="harga_m" value="<?= $harga_m ?>" placeholder="(Rp)">
              </div>

              <div class="form-group">
                <label for="namalengkap"> Harga per Kg</label>
                <input type="number" class="form-control" name="harga_k" value="<?= $harga_k ?>" placeholder="(Rp)">
              </div>
              
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                <?php if($status == "baru"){ echo '<button type="reset" class="btn btn-warning btn-block btn-flat">Batal</button>';?>
                <?php } else { ?> 
                <a href="<?php echo base_url(); ?>kategori" class="btn btn-warning btn-block btn-flat">Batal</a>
                <?php } ?>
              </div><!-- /.col -->
            </form>
          </div><!-- /.item -->
         
        </div><!-- /.chat -->
      </div><!-- /.box (chat box) -->
    </section><!-- /.Left col -->

      <div class="col-md-9">

        <div class="box">
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <div class="box-title">
            
          </div><!-- /.box-title -->
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NO</th>
                <th>JENIS</th>
                <th>HARGA/ METER</th>
                <th>HARGA/ KG</th>
                <th <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>AKSI</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=0; foreach($data_kategori as $row) { $no++ ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row['kategori']; ?></td>
                <td><?php echo currency_format($row['harga_m']); ?></td>
                <td><?php echo currency_format($row['harga_k']); ?></td>
                <td <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
                <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>kategori/editkategori/<?php echo $row['id_kat']; ?>"><i class="fa fa-pencil"></i></a>
                <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-xs" title="hapus data" href="<?php echo base_url(); ?>kategori/hapuskat/<?php echo $row['id_kat']; ?>"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">

    </section><!-- right col -->
  </div><!-- /.row (main row) -->

</section><!-- /.content -->
