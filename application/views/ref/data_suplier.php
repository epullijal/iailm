  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

                <!-- Left col -->
    <section class="col-md-3">
      <!-- Chat box -->
      <div class="box">
        <div class="box-header">
          <i class="fa fa-edit"></i>
          <h3 class="box-title">Form Suplier</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <!-- chat item -->
          <div class="item">
            <form role="form" action="<?php echo base_url(); ?>suplier/savedata" method="post">
              <div class="form-group">
                <label for="nm_suplier">Nama Suplier</label>
                  <input type="text" class="form-control" value="<?php echo $nm_suplier; ?>" id="" name="nm_suplier" placeholder="Isi Nama Suplier" required>
              </div>
              <div class="form-group">
                <label for="kontak">Kontak</label>
                  <input type="number" class="form-control" value="<?php echo $kontak; ?>" id="" name="kontak" placeholder="No. Kontak" required>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" rows="4" id="" name="alamat" placeholder="Alamat Suplier" ><?php echo $alamat; ?></textarea>
              </div>
              <input type="hidden" name="id_suplier" value="<?php echo $id_suplier; ?>" />
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <div class>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                <?php if($status == "baru"){ echo '<button type="reset" class="btn btn-warning btn-block btn-flat">Batal</button>';?>
                <?php } else { ?> 
                <a href="<?php echo base_url(); ?>suplier" class="btn btn-warning btn-block btn-flat">Batal</a>
                <?php } ?>
              </div><!-- /.col -->
            </form>
          </div><!-- /.item -->
         
        </div><!-- /.chat -->
      </div><!-- /.box (chat box) -->
    </section><!-- /.Left col -->

      <div class="col-md-9">

        <div class="box">
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <div class="box-title">
            
          </div><!-- /.box-title -->
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NO</th>
                <th>NAMA SUPLIER</th>
                <th>KONTAK</th>
                <th>ALAMAT</th>
                <th>AKSI</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=0; foreach($data_suplier as $row) { $no++ ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row['nm_suplier']; ?></td>
                <td><?php echo $row['kontak']; ?></td>
                <td><?php echo $row['alamat']; ?></td>
                <td>
                <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>suplier/editSuplier/<?php echo $row['id_suplier']; ?>"><i class="fa fa-pencil"></i></a>
                <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-xs" title="hapus data" href="<?php echo base_url(); ?>suplier/hapussupl/<?php echo $row['id_suplier']; ?>"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">

    </section><!-- right col -->
  </div><!-- /.row (main row) -->

</section><!-- /.content -->
