  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

                <!-- Left col -->
    <section class="col-md-3" <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
      <!-- Chat box -->
      <div class="box">
        <div class="box-header">
          <i class="fa fa-edit"></i>
          <h3 class="box-title">Form Data Pengeluaran</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <!-- chat item -->
          <div class="item">
            <form role="form" action="<?php echo base_url(); ?>ref/savepengeluaran" method="post">
              <div class="form-group">
                <label for="kd_pengeluaran">Kode Pengeluaran</label>
                  <input type="text" class="form-control" id="kd_pengeluaran" name="kd_pengeluaran" placeholder="Kode Pengeluaran" required value="<?php echo $kd_pengeluaran ?>">
                  <span id="pesan-error-flash"><?php echo $this->session->flashdata('kd_pengeluaran'); ?></span>
              </div>

              <div class="form-group">
                <label for="nm_pengeluaran">Jenis Pengeluaran</label>
                  <input type="text" class="form-control" id="" name="nm_pengeluaran" placeholder="Isi Jenis Pengeluaran" required value="<?php echo set_value('nm_pengeluaran');?><?php echo $nm_pengeluaran ?>">
              </div>
              
              <input type="hidden" name="id_pengeluaran" value="<?php echo $id_pengeluaran; ?>" />
              <input type="hidden" name="jns" value="k" />
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                <?php if($status == "baru"){ echo '<button type="reset" class="btn btn-warning btn-block btn-flat">Batal</button>';?>
                <?php } else { ?> 
                <a href="<?php echo base_url(); ?>ref/pengeluaran" class="btn btn-warning btn-block btn-flat">Batal</a>
                <?php } ?>
              </div><!-- /.col -->
            </form>
          </div><!-- /.item -->
         
        </div><!-- /.chat -->
      </div><!-- /.box (chat box) -->
    </section><!-- /.Left col -->

      <div class="col-md-9">

        <div class="box">
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <div class="box-title">
            
          </div><!-- /.box-title -->
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NO</th>
                <th>KODE PENGELUARAN</th>
                <th>JENIS PENGELUARAN</th>
                <th <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>AKSI</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=0; foreach($data_pengeluaran as $row) { $no++ ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row['kd_transaksi']; ?></td>
                <td><?php echo $row['nm_transaksi']; ?></td>
                <td <?php if($this->session->userdata('level')==2){echo "hidden=\"true\"";}else{echo "";} ?>>
                <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>ref/editpengeluaran/<?php echo $row['id_transaksi']; ?>"><i class="fa fa-pencil"></i></a>
                <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-xs" title="hapus data" href="<?php echo base_url(); ?>ref/hapuspengeluaran/<?php echo $row['id_transaksi']; ?>"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">

    </section><!-- right col -->
  </div><!-- /.row (main row) -->

</section><!-- /.content -->
