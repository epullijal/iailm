  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

                <!-- Left col -->
    <section class="col-md-3">
      <!-- Chat box -->
      <div class="box">
        <div class="box-header">
          <i class="fa fa-edit"></i>
          <h3 class="box-title">Form Data</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <!-- chat item -->
          <div class="item">
            <form role="form" action="<?php echo base_url(); ?>perusahaan/<?php if($status=="baru"){echo "tambah_data";}else{echo "update_data";} ?>" method="post">
              <div class="form-group">
                <label for="nama_perusahaan">Nama Lembaga</label>
                  <input type="text" class="form-control" value="<?php echo $nama_perusahaan; ?>" id="" name="nama_perusahaan" placeholder="Isi Nama Perusahaan" required>
              </div>
              <div class="form-group">
                <label for="telp">Telpon</label>
                  <input type="text" class="form-control" value="<?php echo $telp; ?>" id="" name="telp" placeholder="No. telpon" required>
              </div>
              <div class="form-group">
                <label for="email">e-mail</label>
                  <input type="e-mail" class="form-control" value="<?php echo $email; ?>" id="" name="email" placeholder="e-mail" required>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" rows="4" id="" name="alamat" placeholder="Alamat Perusahaan" ><?php echo $alamat; ?></textarea>
              </div>
              
              <div class="form-group" hidden="">
                <label for="foto">Logo</label>
                <input type="text" name="foto" value="<?php echo $foto; ?>">
                <input type="file" name="userfile">
                  <img width="70" src="<?php echo base_url();?>assets/images/<?php echo $foto; ?>">
              </div>
              <input type="hidden" name="id_perusahaan" value="<?php echo $id_perusahaan; ?>" />
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <div class>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                <?php if($status == "baru"){ echo '<button type="reset" class="btn btn-warning btn-block btn-flat">Batal</button>';?>
                <?php } else { ?> 
                <a href="<?php echo base_url(); ?>perusahaan" class="btn btn-warning btn-block btn-flat">Batal</a>
                <?php } ?>
              </div><!-- /.col -->
            </form>
          </div><!-- /.item -->
         
        </div><!-- /.chat -->
      </div><!-- /.box (chat box) -->
    </section><!-- /.Left col -->

      <div class="col-md-9">

        <div class="box">
          <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
          <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <div class="box-header">
          <i class="fa fa-file-o"></i>
          <h3 class="box-title">Data Lembaga</h3>
        </div>
          <div class="box-body table-responsive">
           <table id="" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NAMA PERUSAHAAN</th>
                <th>TELPON</th>
                <th>E-MAIL</th>
                <th>ALAMAT</th>
                <th>LOGO</th>
                <th>EDIT</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($data_perusahaan as $row) { ?>
              <tr>
                <td><?php echo $row['nama_perusahaan']; ?></td>
                <td><?php echo $row['telp']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['alamat']; ?></td>
                <td><img width="70" src="<?= base_url();?>assets/images/<?php echo $row['foto'];?>"></td>
                <td>
                <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>perusahaan/editPerusahaan/<?php echo $row['id_perusahaan']; ?>"><i class="fa fa-pencil"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">

    </section><!-- right col -->
  </div><!-- /.row (main row) -->

</section><!-- /.content -->
