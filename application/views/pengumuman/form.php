<div class="row">
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
        <i class="fa fa-edit"></i>
          <h3 class="box-title">Form</h3>
          <div class="box-tools pull-right">
            <a href="<?php echo base_url(); ?>pengumuman/" class="btn btn-warning btn-flat">Kembali</a>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>pengumuman/savedata" method="post">
            <input type="hidden" name="id_pengumuman" value="<?php echo $id_pengumuman; ?>" />
            <input type="hidden" name="status" value="<?php echo $status; ?>" />

          <div class="form-group">
            <label class="control-label col-md-2">Judul Pengumuman</label>
            <div class="col-md-5">
            <input type="text" name="judul_pengumuman" placeholder="max 50 karakter" class="form-control" value="<?php echo $judul_pengumuman; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Penulis</label>
            <div class="col-md-5">
            <input type="text" name="penulis" placeholder="max 50 karakter" class="form-control" value="<?php echo $penulis; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Isi Pengumuman</label>
            <div class="col-md-8">
            <textarea class="textareaberita" name="isi_pengumuman" placeholder="Isi Pengumuman" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $isi_pengumuman; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Status</label>
            <div class="col-md-5">
            <input type="radio" name="status_post" value="0" <?php echo ($status_post ==0) ? "checked" : ""; ?>> Draft<br>
            <input type="radio" name="status_post" value="1" <?php echo ($status_post ==1) ? "checked" : ""; ?>> Publish
            </div>
          </div>

          <hr>
          <div class="form-group">
            <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            <button type="reset" class="btn btn-default btn-flat">Reset</button>
            <a href="<?php echo base_url(); ?>berita/" class="btn btn-warning btn-flat">Batal</a>
            </div>
          </div>


          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->

</section><!-- right col -->
</div><!-- /.row (main row) -->