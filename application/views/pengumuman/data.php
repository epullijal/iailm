<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
      <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
      <div class="box">
        <div class="box-header with-border">
          <a href="<?= base_url()?>pengumuman/form" class="btn btn-default"> <i class="fa fa-plus-circle"> </i> Tambah pengumuman</a>
          <form style="margin-right: 30px;" class="pull-right" action="<?= base_url(); ?>pengumuman/cari" method="get">
            <input type="text" class="" name="judul_pengumuman" placeholder="Cari Judul" required="" size="25" style="border-radius: 0; height: 32px;">
            <button type="submit" class="btn btn-default btn-flat">Cari</button>
          </form>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
         <table id="" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Tgl Dibuat</th>
              <th>Tgl Diedit</th>
              <th>Judul</th>
              <th>Isi Pengumuman</th>
              <th>Penulis</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=0; foreach($data_pengumuman as $row) { $no++ ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row->tgl_buat; ?></td>
              <td><?php echo $row->tgl_edit; ?></td>
              <td><?php echo substr($row->judul_pengumuman, 0,20); ?>...</td>
              <td><?php echo substr($row->isi_pengumuman, 0,30); ?>...</td>
              <td><?php echo $row->penulis; ?></td>
              <td><?php if($row->status_post==0){echo "<span class='label label-warning'>Draft</span>";}else{echo "<span class='label label-success'>Published</span>";} ?></td>
              <td>
              <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>pengumuman/edit_pengumuman/<?php echo $row->id_pengumuman; ?>"><i class="fa fa-pencil"></i></a>
              <a onclick="return confirm('Seluruh data Pembelian, Pencetakan dan pengiriman akan dihapus!');" class="btn btn-danger btn-xs" title="hapus data" href="<?php echo base_url(); ?>pengumuman/hapus_pengumuman/<?php echo $row->id_pengumuman; ?>"><i class="fa fa-trash"></i></a>
              <a class="btn btn-info btn-xs" title="lihat detail data" href="<?php echo base_url(); ?>pengumuman/detail/<?php echo $row->id_pengumuman; ?>"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php echo $this->pagination->create_links(); ?>
        <hr>
      </div> <!-- boxbody -->
    </div><!-- /.box -->
  </div> <!-- row -->
  <!-- right col (We are only adding the ID to make the widgets sortable)-->
</section> <!-- content -->