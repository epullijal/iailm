<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
      <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
      <div class="box">
        <div class="box-header with-border">
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
         <table id="" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Tgl Diedit</th>
              <th>Judul</th>
              <th>Deskripsi</th>
              <th>Penulis</th>
              <th>Ubah</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=0; foreach($data_pimpinan as $row) { $no++ ?>
            <tr>
              <td><?php echo $no; ?></td>
m              <td><?php echo $row['tgl_edit']; ?></td>
              <td><?php echo $row['judul']; ?></td>
              <td><?php echo substr($row['deskripsi'], 0,30); ?>...</td>
              <td><?php echo $row['penulis']; ?></td>
              <td>
              <a class="btn btn-warning btn-xs" title="edit data" href="<?php echo base_url(); ?>profil/edit_pimpinan/<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i></a>
              <a class="btn btn-info btn-xs" title="lihat detail data" href="<?php echo base_url(); ?>profil/detail_pimpinan/<?php echo $row['id']; ?>"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <hr>
      </div> <!-- boxbody -->
    </div><!-- /.box -->
  </div> <!-- row -->
  <!-- right col (We are only adding the ID to make the widgets sortable)-->
</section> <!-- content -->