<div class="row">
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
        <i class="fa fa-edit"></i>
          <h3 class="box-title">Form</h3>
          <div class="box-tools pull-right">
            <a href="<?php echo base_url(); ?>profil/table_pimpinan" class="btn btn-warning btn-flat">Kembali</a>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>profil/save_pimpinan" method="post">
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
            <input type="hidden" name="status" value="<?php echo $status; ?>" />

          <div class="form-group">
            <label class="control-label col-md-2">Judul</label>
            <div class="col-md-5">
            <input type="text" name="judul" class="form-control" value="<?php echo $judul; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Penulis</label>
            <div class="col-md-5">
            <input type="text" name="penulis" placeholder="max 50 karakter" class="form-control" value="<?php echo $penulis; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Deskripsi</label>
            <div class="col-md-8">
            <textarea class="textareaberita" name="deskripsi" placeholder="Isi Pengumuman" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $deskripsi; ?></textarea>
            </div>
          </div>

          <hr>
          <div class="form-group">
            <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            <button type="reset" class="btn btn-default btn-flat">Reset</button>
            <a href="<?php echo base_url(); ?>profil/table_pimpinan" class="btn btn-warning btn-flat">Batal</a>
            </div>
          </div>


          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->

</section><!-- right col -->
</div><!-- /.row (main row) -->