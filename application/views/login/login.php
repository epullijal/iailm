<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Halaman Login | Aplikasi HTR</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/dist/css/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <!-- <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />-->
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" /
        <!-- Theme style -->
    <style type="text/css">
      .login-box-body{
        box-shadow: 5px 5px 30px black;
      }
    </style>

  </head>
  <body class="login-page" style="background-image: url(assets/images/green-background.png);background-size: 100%;background-repeat: no-repeat;">
    <div class="login-box">
      <div class="login-logo">
      </div><!-- /.login-logo -->
      <div class="login-box-body" align="center">
      <img src="<?= base_url();?>assets/images/logo-iailm.png" width="60">
      <p style="font-size:24px;color:#777777;"><i class="fa fa-key"> </i> <b>Silahkan Login</b></p>
      <hr style="border-color:#00a65a">

        <form action="<?php echo base_url(); ?>login/proseslog" id="form-login" method="post" accept-charset="utf-8">
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            <input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Username" required autofocus="" />
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input type="password" class="form-control" value="" name="pass_user" id="pass_user" placeholder="Password" required/>
          </div>
            <hr style="border-color:#00a65a">
          <div class="row">
            <div class="col-xs-4">
              <input type="submit" name="login" value="Login" id="submit-login" class="btn btn-success btn-block btn-flat" />
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    </body>
    <!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    

  
</html>