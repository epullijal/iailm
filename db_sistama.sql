-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2016 at 05:46 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sistama`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id_berita` int(15) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `judul_berita` varchar(50) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_edit` datetime NOT NULL,
  `isi_berita` longtext NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `status_post` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_berita`
--

INSERT INTO `tb_berita` (`id_berita`, `foto`, `judul_berita`, `tgl_buat`, `tgl_edit`, `isi_berita`, `penulis`, `status_post`) VALUES
(1, '13322178_1129033157117176_7327086640096916302_n1.jpg', 'Sesepuh Pondok Pesantren Suryalaya', '2016-12-08 05:13:14', '2016-12-17 22:08:12', 'KH. Ahmad Shohibulwafa Tajul `Arifin merupakan sesepuh Pondok Pesantren Suryalaya&nbsp;sekaligus Guru Mursyid Tarekat Qodiriyah Naqsyabandiyah Suryalaya.<br><br>Beliau merupakan anak dari KH. Abdullah Mubarok bin Nur Muhammad ra.<br><br>Indonesia tanah airku,<br>tanah tumpah darahku<br>disanalah aku berdiri<br>jadi pandu ibuku.<br><br>Indonesia kebangsaanku<br>bangsa dan tanah airku<br>marilah kita berseru<br>Indonesia bersatu.<br><br>Hiduplah tanahku&nbsp;<br>hiduplah negeriku<br>bangsaku rakyatku semuanya<br><br>bangunlah jiwanya<br>bangunlah badannya<br>untuk indonesia raya', 'epullijal', 1),
(2, 'Aceng_tad_322096.jpeg', 'Peletakan Batu Pertama Kampus IAILM', '2016-12-01 11:28:32', '2016-12-17 13:37:15', 'Indonesia tanah airku<br>tanah tumpah darahku<br>disanalah aku berdiri&nbsp;<br>jadi pandu ibuku<br><br>Indonesia', 'epullijal', 1),
(3, 'Akiki_255687.jpeg', 'Abah Anom dan Para Wakil Talkin', '2016-12-17 22:11:25', '0000-00-00 00:00:00', 'Abah Anom (KH. Ahmad Shohibul Wafa Tajul Arifin) sampai saat ini masih memiliki 57 orang Wakil Talkin yang berada di berbagai daerah termasuk luar Negeri.<br><br><br>', 'epullijal', 1),
(4, 'www.png', 'Peremajaan Website IAILM', '2016-12-17 22:14:49', '0000-00-00 00:00:00', 'Seiring dengan perkembangan zaman dan perkembangan teknologi berbasis internet, maka dipandang perlu untuk mengoptimalkan fungsi dari website IAILM sebagai pusat informasi bagi siapapun yang membutuhkan pengetahuan tentang IAILM.', 'epullijal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE `tb_login` (
  `id_user` int(2) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `pass_user` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `level` enum('1','2','3','4','5','6','7','8') NOT NULL,
  `status` enum('1','0') NOT NULL,
  `bu_pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_login`
--

INSERT INTO `tb_login` (`id_user`, `nama_user`, `pass_user`, `nama`, `level`, `status`, `bu_pass`) VALUES
(1, 'asep', 'f2454fc1a181cd2ccc8d3e486edc233b', 'Dr. Asep Saeful Rijal, M.Cod', '1', '1', 'ij4ldotkom');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(15) NOT NULL,
  `judul_pengumuman` varchar(50) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_edit` datetime NOT NULL,
  `isi_pengumuman` longtext NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `status_post` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id_pengumuman`, `judul_pengumuman`, `tgl_buat`, `tgl_edit`, `isi_pengumuman`, `penulis`, `status_post`) VALUES
(1, 'Penerimaan Calon Mahasiswa Baru TA 2017', '2016-12-17 17:36:41', '2016-12-17 17:39:03', 'Mahasiswa sekarang dapat mendaftar untuk kuliah di IAILM dengan mengunjungi web&nbsp;<a target="_blank" rel="nofollow" href="http://www.iailm.ac.id">http://www.iailm.ac.id/</a>&nbsp;untuk mendownload formulir, dan kemudian dicetak.&nbsp;<br>Setelah diisi secara lengkap, calon pendaftar membayar biaya pendaftaran ke bank BRI dan kemudian menyerahkannya bersama formulir kepada petugas penerimaan Mahasiswa Baru di Kampus IAILM Suryalaya.<br><br>Setelah di ferivikasi oleh petugas, calon pendaftar akan mendapatkan kartu tanda bukti sebagai bukti kelayakan mengikuti ujian tes.', 'epullijal', 1),
(2, 'Pembayaran Biaya Perkuliahan TA 2017', '2016-12-17 21:14:06', '0000-00-00 00:00:00', 'Terhitung mulai tanggal 1 Januari pada Tahun Ajaran 2017 akan mulai diberlakukan sistem baru dalam proses pembayaran biaya perkuliahan.<br><br>Mahasiswa harus melunasi biaya perkuliahan pada awal semester sebelum perkuliahan dimulai demi terciptanya ketertiban dalam proses pencatatan keuangan dan menghindari ketimpangan dalam mendata keuangan.<br><br>berdasarkan pertimbangan tersebut, mahasiswa diberi tenggang waktu untuk melunasi sebanyak 3 kali cicilan sampai paling lambat satu (1) bulan setelah perkuliahan dimulai.', 'Warek II', 1),
(3, 'Jadwal Kerja Staf IAILM TA 2017', '2016-12-17 21:17:17', '0000-00-00 00:00:00', 'Demi pelayanan maksimal kepada mahasiswa, maka melalui Surat Keputusan Rektor No. 12/23/rektor/2016&nbsp;terhitung mulai tanggal 19 Desember 2016 akan diberlakukan Jadwal Kerja baru bagi seluruh Staf dan Karyawan IAILM, yaitu mulai masuk kerja pukul 07:00 dan berakhir pada pukul 15:00 WIB.', 'epullijal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_perusahaan`
--

CREATE TABLE `tb_perusahaan` (
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `alamat` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_perusahaan`
--

INSERT INTO `tb_perusahaan` (`id_perusahaan`, `nama_perusahaan`, `foto`, `telp`, `email`, `alamat`) VALUES
(1, 'Institut Agama Islam Latifah Mubarokiyah', 'logo-iailm.png', '0265 455808', 'iailm@suryalaya.org', 'Jl. Suryalaya, Desa Tanjungkerta, Kec. Pagerageung, Kab. Tasikmalaya 46158 Jawa Barat');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profil`
--

CREATE TABLE `tb_profil` (
  `id` int(5) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_edit` datetime NOT NULL,
  `deskripsi` longtext NOT NULL,
  `penulis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profil`
--

INSERT INTO `tb_profil` (`id`, `judul`, `tgl_buat`, `tgl_edit`, `deskripsi`, `penulis`) VALUES
(1, 'Pimpinan', '0000-00-00 00:00:00', '2016-12-27 17:18:23', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_perusahaan`
--
ALTER TABLE `tb_perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `tb_profil`
--
ALTER TABLE `tb_profil`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id_berita` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_login`
--
ALTER TABLE `tb_login`
  MODIFY `id_user` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_perusahaan`
--
ALTER TABLE `tb_perusahaan`
  MODIFY `id_perusahaan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_profil`
--
ALTER TABLE `tb_profil`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
